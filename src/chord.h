/* chord.h - a free guitar chord practising program

   Copyright (C) 2012 Ole Aamot <oka+guitar@oka.no>

   This program is in the public domain.
*/

#ifndef _CHORD_H_
#define _CHORD_H_

typedef  int (*_show) (char *name, int e1, int b2, int g3, int d4, int a5, int e6);
typedef  int (*_play) (char *name);
typedef void (*_fret) (int fret);
typedef void (*_text) (char *lyrics);

struct Display {
	_show show;
	_play play;
	_fret fret;
	_text text;
};

struct Chord {
	char *root;
	struct Display *ops;
	int e1;
	int b2;
	int g3;
	int d4;
	int a5;
	int e6;
};

void chord_text(char *chord, char *lyric);
void chord_loop(void);
 int chord_play(char *chord);
void chord_main(int argc, char **argv);
void chord_fail(char *chord);

#endif /* _CHORD_H_ */
