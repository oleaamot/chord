/*  chord - a free guitar chord finder program

    Copyright (C) 2012  Ole Aamot

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <gst/player/player.h>
#include "chord.h"

void console_text(char *lyrics)
{
	fprintf(stdout, "%s\n", lyrics);
}

int console_play(char *root)
{
        GstPlayer *player;
        player = gst_player_new (NULL, gst_player_g_main_context_signal_dispatcher_new(NULL));
	fprintf(stdout, root);
        gst_player_set_uri(player, root);
        gst_player_play(player);
        return (0);
}

void console_fret(int fret)
{

	if (fret == -1) {
		fprintf(stdout, "-x-|-x-|-x-|-x-|-x-|-x-|-x-|-x-|-x-|-x-|-x-|-x-|-x-|-x-|-x-|-x-|-x-\n");
	}
	if (fret == 0) {
		fprintf(stdout, "---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---\n");
	}
	if (fret == 1) {
		fprintf(stdout, "-1-|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---\n");
	}
	if (fret == 2) {
		fprintf(stdout, "---|-2-|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---\n");
	}
	if (fret == 3) {
		fprintf(stdout, "---|---|-3-|---|---|---|---|---|---|---|---|---|---|---|---|---|---\n");
	}
	if (fret == 4) {
		fprintf(stdout, "---|---|---|-4-|---|---|---|---|---|---|---|---|---|---|---|---|---\n");
	}
	if (fret == 5) {
		fprintf(stdout, "---|---|---|---|-5-|---|---|---|---|---|---|---|---|---|---|---|---\n");
	}
	if (fret == 6) {
		fprintf(stdout, "---|---|---|---|---|-6-|---|---|---|---|---|---|---|---|---|---|---\n");
	}
	if (fret == 7) {
		fprintf(stdout, "---|---|---|---|---|---|-7-|---|---|---|---|---|---|---|---|---|---\n");
	}
	if (fret == 8) {
		fprintf(stdout, "---|---|---|---|---|---|---|-8-|---|---|---|---|---|---|---|---|---\n");
	}
	if (fret == 9) {
		fprintf(stdout, "---|---|---|---|---|---|---|---|-9-|---|---|---|---|---|---|---|---\n");
	}
	if (fret == 10) {
		fprintf(stdout, "---|---|---|---|---|---|---|---|---|-10|---|---|---|---|---|---|---\n");
	}
	if (fret == 11) {
		fprintf(stdout, "---|---|---|---|---|---|---|---|---|---|-11|---|---|---|---|---|---\n");
	}
	if (fret == 12) {
		fprintf(stdout, "---|---|---|---|---|---|---|---|---|---|---|-12|---|---|---|---|---\n");
	}
	if (fret == 13) {
		fprintf(stdout, "---|---|---|---|---|---|---|---|---|---|---|---|-13|---|---|---|---\n");
	}
	if (fret == 14) {
		fprintf(stdout, "---|---|---|---|---|---|---|---|---|---|---|---|---|-14|---|---|---\n");
	}
	if (fret == 15) {
		fprintf(stdout, "---|---|---|---|---|---|---|---|---|---|---|---|---|---|-15|---|---\n");
	}
	if (fret == 16) {
		fprintf(stdout, "---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|-16|---\n");
	}
	if (fret == 17) {
		fprintf(stdout, "---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|-17\n");
	}
	return;
}

int console_show(char *root, int e1, int b2, int g3, int d4, int a5, int e6)
{

	fprintf(stdout, "\nFound chord %s\n\n", root);
	console_fret(e1);
	console_fret(b2);
	console_fret(g3);
	console_fret(d4);
	console_fret(a5);
	console_fret(e6);
	console_play(g_strconcat("file://", CHORD_DATADIR, "/", root, ".ogg", NULL));
	fprintf(stdout, "\n", root);
	return (1);
}

struct Display console = {
	console_show,
	console_play,
	console_fret,
	console_text
};
